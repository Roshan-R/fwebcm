# fwebcm

Play videos as your webcam feed !

## Installing dependencies

 use ` sudo apt install v4l2loopback-dkms ffmpeg zenity ` to install dependencies
 
 ## Running
 
 make the script executable and enjoy!
 ```
 chmod +x webcam.sh 
 ./webcam.sh
 ```
